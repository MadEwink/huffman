import csv

#freq=[('a',0.54),('b':0.42)]
#codage=dict()

def sum_freq(freq, i1, i2):
    result=0
    for i in freq[i1:i2+1]:
        result += i[1]
    return result

def recur_huff(freq, inf1, sup2, codage):
    sup1 = inf1
    inf2 = sup2
    while(inf2-sup1 > 1):
        if (sum_freq(freq,inf1, sup1) < sum_freq(freq,inf2,sup2)):
            sup1 += 1
        else:
            inf2 -= 1
    for i in range(inf1, sup1+1):
        codage[freq[i][0]] += "0"
    for i in range(inf2, sup2+1):
        codage[freq[i][0]] += "1"
    if (sup1-inf1 > 0):
        recur_huff(freq, inf1, sup1, codage)
    if (sup2-inf2 > 0):
        recur_huff(freq, inf2, sup2, codage)

def read_csv_freq(file_name):
    with open(file_name, "r") as f:
        freq = list()
        spamreader = csv.reader(f, delimiter=';')
        for row in spamreader:
            freq.append((row[0],float(row[1])))
        return freq

freq = read_csv_freq("frequencies.csv")
#print(freq)
codage = dict()
for i in freq:
    codage[i[0]] = ""
recur_huff(freq, 0, len(freq)-1, codage)
for i in codage:
    print(i,codage[i], sep=";")
